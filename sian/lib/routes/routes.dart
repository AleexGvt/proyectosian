import 'package:flutter/material.dart';
import 'package:sian/pages/home.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {

  return <String, WidgetBuilder> {
    '/' : (BuildContext context) => HomePage(),

  };
}