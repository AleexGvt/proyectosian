import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       /*
       home: DefaultTabController(
         length: 
          child: Scaffold(
       appBar: AppBar(
        backgroundColor: Color.fromRGBO(0, 0, 255, 1),
        leading:  Icon(
             Icons.local_bar
            ),
        centerTitle: true,
        bottom: TabBar(
          isScrollable: true,
          tabs: choices.map((Choice choice)){

          }
        ),
      ),
    ),
    )
*/
     
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(0, 0, 255, 1),
            leading: Icon(Icons.local_bar,color: Colors.white,),
          ),
          body: new DefaultTabController(
          length: 3,
          child: new Column(
            children: <Widget>[
              new Container(
                constraints: BoxConstraints(maxHeight: 150.0),
                child: new Material(
                  color: Colors.white,
                  child: new TabBar(
                    labelColor: Colors.black12,
                    tabs: [
                      new Tab(text: 'Neumaticos',),
                      new Tab(text: 'Profundidad',),
                      new Tab(text: 'Datos'),
                    ],
                  ),
                ),
              ),
              new Expanded(
                child: new TabBarView(
                  children: [
                    _formulario(),
                    _profundidades(),
                    new Icon(Icons.directions_bike),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget _profundidades(){
  return Scaffold(
 body: Padding(
         padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  children: <Widget>[
                  Text('Toma 1'),
                  SizedBox(height: 10,),
                  _crearExterna(),
                  SizedBox(height: 15,),
                  _crearCentral(),
                  SizedBox(height: 15,),
                  _crearExterna(),
                  SizedBox(height: 10,),
                  Text('Toma 2'),
                  SizedBox(height: 10,),
                  _crearExterna(),
                  SizedBox(height: 15,),
                  _crearCentral(),
                  SizedBox(height: 15,),
                  _crearExterna(),
                  SizedBox(height: 10,),
                  Text('Toma 3'),
                  SizedBox(height: 10,),
                  _crearExterna(),
                  SizedBox(height: 15,),
                  _crearCentral(),
                  SizedBox(height: 15,),
                  _crearExterna(),
                  SizedBox(height: 10,),
                  ],
      ),
    ),
  ),
 ),
);

}

Widget _formulario(){
return Scaffold(
 body: Padding(
         padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: SingleChildScrollView(
              child: Center(
                child: Column(
                  children: <Widget>[
                    _crearCodigo(),
                    SizedBox(height: 15,),
                    _crearPosicion(),
                    SizedBox(height: 15,),
                    _crearPresion(),
                    SizedBox(height: 15,),
                    _crearKilometraje(),
                  ],
      ),
    ),
  ),
 ),
);
}

 Widget _crearCodigo() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Codigo del Neumatico',
            hintText: '750145687728',
          ),
          onChanged: (texto){
          }
        ),
    );
      }
    );
  }

Widget _crearPosicion() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Posicion',
            hintText: 'Delantera',
          ),
          onChanged: (texto){
          }
        ),
    );
      }
    );
  }

Widget _crearPresion() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Presion del Neumatico',
            hintText: '75',
          ),
          onChanged: (texto){
          }
        ),
    );
      }
    );
  }

  Widget _crearKilometraje() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Kilometraje del Neumatico',
            hintText: '750145 km',
          ),
          onChanged: (texto){
          }
        ),
    );
      }
    );
  }

Widget _crearExterna() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Profundidad Externa del Neumatico',
            hintText: '750145687728',
          ),
          onChanged: (texto){
          }
        ),
    );
      }
    );
  }

  Widget _crearCentral() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Profundidad Central del Neumatico',
            hintText: '750145687728',
          ),
          onChanged: (texto){
          }
        ),
    );
      }
    );
  }

  Widget _crearInterna() {

    return StreamBuilder(
      builder: (BuildContext context, AsyncSnapshot snapshot){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: TextField(
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5.0)
            ),
            labelText: 'Profundidad Interna del Neumatico',
            hintText: '750145687728',
          ),
          onChanged: (texto){
          }
        ),
    );
      }
    );
  }
 
